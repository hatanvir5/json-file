let Shameem = {};

Shameem.Todos = {

     getTodosData:function(data) {
        let txt = document.createTextNode(data);
        let span = document.createElement("span");
        span.appendChild(txt);
        return span;
      },
       createInput:function(InputData) {
        let input = document.createElement("input");
        input.setAttribute("type", "checkbox");
        if (InputData === true) {
          input.setAttribute("checked", "checked");
        }
        return input;
      },
       getTodo:function(todo) {
        let div = document.createElement("div");
        for (let data of todo) {
          let ul = document.createElement("ul");
          ul.setAttribute(
            "style",
            `width:360px;
             height:100px;
             background-color:#009688;
             display:inline-block;
             margin:20px;
             padding:20px;`
          );
          let li1 = document.createElement("li");
          let li2 = document.createElement("li");
          let li3 = document.createElement("li");
          let li4 = document.createElement("li");
      
          li1.appendChild(this.createInput(data.id));
          li1.appendChild(this.getTodosData(data.id));
      
          li2.appendChild(this.createInput(data.userId));
          li2.appendChild(this.getTodosData(data.userId));
      
          li3.appendChild(this.createInput(data.title));
          li3.appendChild(this.getTodosData(data.title));
      
          li4.appendChild(this.createInput(data.completed));
          li4.appendChild(this.getTodosData(data.completed));
          ul.appendChild(li1);
          ul.appendChild(li2);
          ul.appendChild(li3);
          ul.appendChild(li4);
          div.appendChild(ul);
        }
        return div;
      },

       display:function(location, todos) {
        let container = document.getElementById(location);
        container.appendChild(this.getTodo(todos));
      },
       displayDataByFetch:function() {
        let url = "https://jsonplaceholder.typicode.com/todos";
        fetch(url)
          .then(res => res.json())
          .then(usersTodo => Shameem.Todos.display("root", usersTodo));
      }
      
}

document.getElementById("btn").addEventListener("click", Shameem.Todos.displayDataByFetch);