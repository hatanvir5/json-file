const Tanvir={}
Tanvir.Photos={

    getPhotosThumbnail:function(thumbnailUrl){

        let photoThumbnailContainer=document.createElement('img')
        photoThumbnailContainer.setAttribute('src',thumbnailUrl)
        return photoThumbnailContainer;
       
    },

getPhotosTitle:function(title){

    let photoTitleContainer=document.createElement('h2')
    let photoTitleTextnode=document.createTextNode(title)
    photoTitleContainer.appendChild(photoTitleTextnode)
    return photoTitleContainer;
},

getPhotosLink:function(url){

    let photoLinkContainer=document.createElement('a')
    photoLinkContainer.setAttribute('href',url)
    let linkLabel=document.createTextNode('Click here ...')
    photoLinkContainer.appendChild(linkLabel)
    return photoLinkContainer;
   
},

getPhotos:function(getPhotoData){
    let container=document.createElement('div')
    let figure=document.createElement('figure')
    let figcaption=document.createElement('figcaption')

    let photoThumnail=this.getPhotosThumbnail(getPhotoData.thumbnailUrl)
    figure.appendChild(photoThumnail)

    let photoTitle=this.getPhotosTitle(getPhotoData.title)
    let photoLink=this.getPhotosLink(getPhotoData.url)
    figcaption.appendChild(photoTitle)
    figcaption.appendChild(photoLink)


    figure.appendChild(figcaption)
    container.appendChild(figure)
    let fieldset=document.createElement('fieldset')
    fieldset.appendChild(container)
    return fieldset;
},

display:function(location,getPhotosData){
let htmlLocation=document.querySelector(location)
for( let getPhotoData of getPhotosData){
    let photo=this.getPhotos(getPhotoData)
    htmlLocation.appendChild(photo)
}
}
}


function loadData(){
    fetch('https://jsonplaceholder.typicode.com/photos')
    .then(response => response.json())
    .then(function (photos) {
        Tanvir.Photos.display('#root', photos);
    })

}
