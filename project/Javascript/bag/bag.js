const Tanvir={}
Tanvir.Bag={
    getBagName:function(name){
        let bagNameContainer=document.createElement('li')
        let bagNameTextnode=document.createTextNode(name)
        bagNameContainer.appendChild(bagNameTextnode)
        return bagNameContainer;
    },
    getBagColor:function(colour){
        let bagColorContainer=document.createElement('li')
        let bagColorTextnode=document.createTextNode(colour)
        bagColorContainer.appendChild(bagColorTextnode)
        return bagColorContainer;
            },

    getBagsData:function(bagData){
        let container=document.createElement('ul')
        container.appendChild(this.getBagName(bagData.name))
        container.appendChild(this.getBagColor(bagData.colour))
        return container;
            },
    display:function(location,bagsData){
        let htmlLocation =document.querySelector(location)
        for(let bagData of bagsData){
            let bagInfo=this.getBagsData(bagData)
            htmlLocation.appendChild(bagInfo)
        }

    }        

}

    fetch('http://localhost/json/bags/bags.json')
    .then(response => response.json())
    
    .then(function (bags) {
    console.log(bags)
    
     // Tanvir.Bag.display('#root', bags);
    })


